﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.Search;
using Tunynet.Tasks;

namespace Tunynet.Common
{
    /// <summary>
    /// 评论索引任务
    /// </summary>
    public class CommentSearchTask : ITask
    {
        /// <summary>
        /// 任务执行的内容
        /// </summary>
        /// <param name="taskDetail">任务配置状态信息</param>
        public void Execute(TaskDetail taskDetail)
        {
            CommentSearcher commentSearcher = (CommentSearcher)SearcherFactory.GetSearcher(CommentSearcher.CODE);

            commentSearcher.SearchTask();
        }
    }
}