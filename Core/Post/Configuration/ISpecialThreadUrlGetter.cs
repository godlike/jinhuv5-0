﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using System;

namespace Tunynet.Post
{
    /// <summary>
    /// 特殊帖子相关信息
    /// </summary>
    public interface ISpecialThreadUrlGetter
    {
        /// <summary>
        /// 帖子类型
        /// </summary>
        ThreadType ThreadType { get; }


        /// <summary>
        /// 获取特殊帖子的信息
        /// </summary>
        /// <param name="associateId"></param>
        /// <param name="threadId">为了跳转到具体的某一个评论</param>
        /// <returns></returns>
        ThreadObjectData GetThreadObject(long associateId);
    }

    public class ThreadObjectData
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public int  AttendCount { get; set; }

        public DateTime StartTime { get; set; }
    }
}