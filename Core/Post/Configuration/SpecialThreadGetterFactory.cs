﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;

namespace Tunynet.Post
{
    /// <summary>
    /// 特殊帖子相关信息
    /// </summary>
    public static class SpecialThreadGetterFactory
    {
        /// <summary>
        /// 依据threadType获取ISpecialThreadGetter
        /// </summary>
        /// <returns></returns>
        public static ISpecialThreadUrlGetter Get(ThreadType threadType)
        {
            return DIContainer.Resolve<IEnumerable<ISpecialThreadUrlGetter>>().Where(g => g.ThreadType== threadType).FirstOrDefault();
        }
    }
}