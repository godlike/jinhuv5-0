﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using Aop.Api;
using Aop.Api.Request;
using Aop.Api.Response;
using RestSharp;
using System.Web;
using Tunynet.Common;
using Tunynet.Utilities;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 支付宝获取器
    /// </summary>
    public class AliPayAccountGetter : ThirdAccountGetter
    {
        private RestClient _restClient;

        /// <summary>
        /// 构造函数
        /// </summary>
        public AliPayAccountGetter()
        {
            _restClient = new RestClient("https://graph.qq.com");
        }

        /// <summary>
        /// 名称
        /// </summary>
        public override string AccountTypeName
        {
            get { return "支付宝"; }
        }

        /// <summary>
        /// 官方网站地址
        /// </summary>
        public override string AccountTypeUrl
        {
            get { return "http://open.alipay.com/"; }
        }

        /// <summary>
        /// 帐号类型Key
        /// </summary>
        public override string AccountTypeKey
        {
            get { return AccountTypeKeys.Instance().AliPay(); }
        }

        /// <summary>
        /// 获取第三方网站空间主页地址
        /// </summary>
        /// <param name="identification"></param>
        /// <returns></returns>
        public override string GetSpaceHomeUrl(string identification)
        {
            return string.Format("https://uemprod.alipay.com/user/ihome.htm", identification);
        }

        /// <summary>
        /// 获取身份认证Url
        /// </summary>
        /// <returns></returns>
        public override string GetAuthorizationUrl()
        {
            string getAuthorizationCodeUrlPattern = "https://openauth.alipay.com/oauth2/publicAppAuthorize.htm?app_id={0}&scope=auth_user&redirect_uri={1}";
            return string.Format(getAuthorizationCodeUrlPattern, AccountType.AppKey, WebUtility.UrlEncode(CallbackUrl));
        }

        /// <summary>
        /// 获取身份认证Url(自定义返回页面)
        /// </summary>
        /// <returns></returns>
        public override string GetAuthorizationUrl(string url)
        {
            string getAuthorizationCodeUrlPattern = "https://openauth.alipay.com/oauth2/publicAppAuthorize.htm?app_id={0}&scope=auth_user&redirect_uri={1}";
            return string.Format(getAuthorizationCodeUrlPattern, AccountType.AppKey, WebUtility.UrlEncode(url));
        }

        /// <summary>
        /// 获取当前第三方帐号上的访问授权
        /// </summary>
        /// <param name="Request"></param>
        /// <param name="expires_in">有效期（单位：秒）</param>
        /// <returns></returns>
        public override string GetAccessToken(HttpRequestBase Request, out int expires_in)
        {
            //Step1：通过Authorization Code获取Access Token
            _restClient.Authenticator = null;
            string code = Request.QueryString.GetString("auth_code", string.Empty);

            AlipaySystemOauthTokenRequest request = new AlipaySystemOauthTokenRequest();
            request.GrantType = "authorization_code";
            request.Code = code;
            var client = DIContainer.Resolve<IAopClient>();

            AlipaySystemOauthTokenResponse oauthTokenResponse = client.Execute(request, "accessToken");
            var access_token = oauthTokenResponse.AccessToken;
            int.TryParse(oauthTokenResponse.ExpiresIn, out expires_in);

            return access_token;
        }

        /// <summary>
        /// 获取当前第三方帐号上的用户
        /// </summary>
        /// <param name="accessToken">访问授权</param>
        /// <param name="identification">标识</param>
        /// <returns></returns>
        public override ThirdUser GetThirdUser(string accessToken, string identification = null)
        {
            var client = DIContainer.Resolve<IAopClient>();

            AlipayUserInfoShareRequest request = new AlipayUserInfoShareRequest();

            AlipayUserInfoShareResponse response = client.Execute(request, accessToken);
            if (string.IsNullOrEmpty(identification))
            {
                identification = response.UserId;
            }
            return new ThirdUser
            {
                AccountTypeKey = AccountType.AccountTypeKey,
                Identification = identification,
                AccessToken = accessToken,
                NickName = response.NickName,
                Gender = response.Gender == "M" ? GenderType.Male : GenderType.FeMale,
                UserAvatarUrl = string.IsNullOrEmpty(response.Avatar) ? "" : response.Avatar
            };
        }
    }
}