﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.Common;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 贴子触屏版url获取
    /// </summary>
    public class ThreadTouchScreenlUrlGetter : ITouchScreenUrlGetter
    {
        /// <summary>
        /// 租户类型Id
        /// </summary>
        public string TenantTypeId
        {
            get { return TenantTypeIds.Instance().Thread(); }
        }

        /// <summary>
        /// 获取贴子详情的触屏版url
        /// </summary>
        /// <param name="objectId">对象ID</param>
        /// <returns></returns>
        public string GetTouchScreenDetailUrl(long objectId)
        {
            var touchScreenDetailUrl = "html/threadDetail.html?threadId=";
            var ipUrl = Utility.GetTouchScreenUrl();
            ipUrl = $"{ipUrl}/{touchScreenDetailUrl}{objectId}";
            return ipUrl;
        }
    }
}