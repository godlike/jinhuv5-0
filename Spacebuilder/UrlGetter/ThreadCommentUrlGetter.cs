﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.Common;
using Tunynet.Post;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 贴子 评论 url 获取
    /// </summary>
    public class ThreadCommentUrlGetter : ICommentUrlGetter
    {
        private Authorizer authorizer;
        private IUserService userService;

        public ThreadCommentUrlGetter(Authorizer authorizer, IUserService userService)
        {
            this.authorizer = authorizer;
            this.userService = userService;
        }

        /// <summary>
        /// 租户类型Id
        /// </summary>
        public string TenantTypeId
        {
            get { return TenantTypeIds.Instance().Thread(); }
        }

        /// <summary>
        /// 是否贴子管理员
        /// </summary>
        public bool IsManager(long userId)
        {
            var user = userService.GetUser(userId);
            var result = authorizer.IsPostManager(user);
            return result;
        }

        /// <summary>
        /// 获取被评论对象
        /// </summary>
        /// <param name="commentedObjectId"></param>
        /// <param name="commentId">评论ID(用于 直接跳转到某一个评论)</param>
        /// <returns></returns>
        public CommentedObject GetCommentedObject(long commentedObjectId, long commentId)
        {
            var thread = new ThreadRepository().Get(commentedObjectId);
            if (thread != null)
            {
                CommentedObject commentedObject = new CommentedObject();
                commentedObject.DetailUrl = SiteUrls.Instance().ThreadDetail(commentedObjectId, commentId);
                commentedObject.Name = thread.Subject;
                commentedObject.Author = thread.Author;
                commentedObject.UserId = thread.UserId;
                commentedObject.contentModelName = null;
                return commentedObject;
            }
            return null;
        }
    }
}