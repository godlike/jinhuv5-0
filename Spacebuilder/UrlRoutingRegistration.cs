﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using System.Web.Mvc;

namespace Tunynet.Spacebuilder
{
    public class UrlRoutingRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "ConsoleViews"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            //后台
            context.MapRoute(
                name: "ControlPanel",
                url: "ControlPanel/{action}",
                defaults: new { controller = "ControlPanel", action = "Home" }
            );
            //后台
            context.MapRoute(
                name: "ControlPanelAsk",
                url: "ControlPanelAsk/{action}",
                defaults: new { controller = "ControlPanelAsk", action = "Home" }
            );
            //文库后台
            context.MapRoute(
                name: "ControlPanelDocument",
                url: "ControlPanelDocument/{action}",
                defaults: new { controller = "ControlPanelDocument", action = "ManageDocument" }
            );
            //活动后台
            context.MapRoute(
                name: "ControlPanelEvent",
                url: "ControlPanelEvent/{action}",
                defaults: new { controller = "ControlPanelEvent", action = "ManageEvent" }
            );
            //投票后台
            context.MapRoute(
                name: "ControlPanelVote",
                url: "ControlPanelVote/{action}",
                defaults: new { controller = "ControlPanelVote", action = "ManageVote" }
            );
            //积分商城后台
            context.MapRoute(
             name: "ControlPanelPointMall",
             url: "ControlPanelPointMall/{action}",
             defaults: new { controller = "ControlPanelPointMall", action = "ManagePointMallProducts" }
         );
        }
    }
}